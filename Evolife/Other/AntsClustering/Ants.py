#!/usr/bin/env python
##############################################################################
# EVOLIFE  www.dessalles.fr/Evolife                    Jean-Louis Dessalles  #
#            Telecom ParisTech  2014                       www.dessalles.fr  #
##############################################################################

##############################################################################
# Ants                                                                       #
##############################################################################

""" Collective foraging:
Though individual agents follow erratic paths to find food,
the collective may discover optimal paths.
"""

# In this story, 'ants' move in search for food
# In the absence of pheromone, ants move randomly for some time,
# and then head back toward the colony.
# When they find food, they return to the colony while laying down pheromone.
# If they find pheromone, ants tend to follow it.


#######     NOTE:  this is just a sketch. The programme must be completed to
#######     display appropriate behaviour



import sys
from time import sleep
import random
		
sys.path.append('..')
sys.path.append('../../..')
import Evolife.Scenarii.Parameters			as EPar
import Evolife.Ecology.Observer				as EO
import Evolife.Ecology.Individual			as EI
import Evolife.Ecology.Group				as EG
import Evolife.Ecology.Population			as EP
import Evolife.QtGraphics.Evolife_Window	as EW
import Evolife.Tools.Tools					as ET
import Landscapes

print ET.boost()	# significantly accelerates python on some platforms


# two functions to convert from complex numbers into (x,y) coordinates
c2t = lambda c: (int(round(c.real)),int(round(c.imag))) # converts a complex into a couple
t2c = lambda (x,y): complex(x,y) # converts a couple into a complex

#################################################
# Aspect of ants, food and pheromons on display
#################################################
AntAspect = ('black', 6)	# 6 = size
AntAspectWhenLaden = ('red1', 7)	# 6 = size
FoodAspect = ('yellow', 14)
FoodDepletedAspect = ('brown', 0)
PPAspect = (17, 2)	# 17th colour
NPAspect = ('blue', 2)

pickK = 0.02
putK = 0.08
	
class Ant_Observer(EO.Observer):
	""" Stores global variables for observation
	"""
	def __init__(self, Scenario):
		EO.Observer.__init__(self, Scenario)
		self.CurrentChanges = []	# stores temporary changes
		self.recordInfo('CurveNames', [('yellow', 'Year (each ant moves once a year on average)\n\t\tx\n\t\tAmount of food collected')])
		self.FoodCollected = 0

	def recordChanges(self, Info):
		# stores current changes
		# Info is a couple (InfoName, Position) and Position == (x,y) or a longer tuple
		self.CurrentChanges.append(Info)

	def get_info(self, Slot):
		" this is called when display is required "
		if Slot == 'PlotOrders':	return [('yellow', (self.StepId//Gbl.Parameter('PopulationSize'), self.FoodCollected))]	# curve
		else:	return EO.Observer.get_info(self, Slot)
		
	def get_data(self, Slot):
		if Slot == 'Positions':
			CC = self.CurrentChanges
			# print CC
			self.CurrentChanges = []
			return tuple(CC)
		else:	return EO.Observer.get_data(self, Slot)
		
class LandCell(Landscapes.LandCell):
	""" Defines what's in one location on the ground
	"""

	# Cell content is defined as a triple  (Food, NegativePheromon, PositivePheromon)

	def __init__(self, F=0, NP=0, PP=0):
		Landscapes.LandCell.__init__(self, (0, 0), VoidCell=(0, 0, 0))
		self.setContent((F,NP,PP))

	def clean(self):	
		return self.setContent((0,0,0))

	def food(self, addendum=0):	
		(F,NP,PP) = self.Content()
		if addendum:	self.setContent((F + addendum, NP, PP))
		return F + addendum
		
	def np(self, addendum=0):	
		(F,NP,PP) = self.Content()
		if addendum:	self.setContent((F, self.limit(NP + addendum), PP))
		return NP + addendum

	def pp(self, addendum=0):	
		(F,NP,PP) = self.Content()
		if addendum:	self.setContent((F, NP, self.limit(PP + addendum)))
		return PP + addendum

	def limit(self, Pheromone):
		return min(Pheromone, Gbl.Parameter('Saturation'))
		
	# def __add__(self, Other):
		# # redefines the '+' operation between cells
		# return LandCell(self.food()+Other.food(),
						# self.limit(self.np() + Other.np()),
						# self.limit(self.pp() + Other.pp())

	def evaporate(self):
		# Pheromone evaporation should be programmed about here
		if self.np() > 0:
			self.np(-Gbl.Parameter('Evaporation')) # Repulsive ('negative') pheromone
		if self.pp() > 0:
			self.pp(-Gbl.Parameter('Evaporation')) # Attractive ('positive') Pheromone
		if self.np() <= 0 and self.pp() <= 0:
			self.clean()
			return True
		return False

class FoodSource:
	""" Location where food is available
	"""
	def __init__(self, Name):
		self.Name = Name
		self.FoodAmount = 0
		self.Location = (-1,-1)
		self.Radius = (Gbl.Parameter('FoodSourceSize')+1)//2
		self.Distribution = Gbl.Parameter('FoodQuantity') // ((2*self.Radius+1) ** 2)
		self.Area = []

	def locate(self, Location = None):
		if Location:
			self.Location = Location
		return self.Location

	def __repr__(self):
		return "[%s, %d, %s...]" % (self.Name, self.FoodAmount, str(self.Area)[:22])
		
	
class Landscape(Landscapes.Landscape):
	""" A 2-D grid with cells that contains food or pheromone
	"""
	def __init__(self, Size, NbFoodSources):
		Landscapes.Landscape.__init__(self, Size, CellType=LandCell)

		# Positioning Food Sources
		self.FoodSourceNumber = NbFoodSources
		self.FoodSources = []
		for FSn in range(self.FoodSourceNumber):
			FS = FoodSource('FS%d' % FSn)
			FS.locate((random.randint(0,Size-1),random.randint(0,Size-1)))
			self.FoodSources.append(FS)
			for Pos in self.neighbours(FS.locate(), Radius=5):
				FS.Area.append(Pos)
				self.food(Pos, FS.Distribution)  # Cell contains a certain amount of food
			Observer.recordChanges((FS.Name, FS.locate() + FoodAspect))	# to display food sources

	# def Modify(self, (x,y), Modification):
		# self.Ground[x][y] += Modification   # uses addition as redefined in LandCell
		# return self.Ground[x][y]

	# def FoodSourceConsistency(self):
		# for FS in self.FoodSources:
			# amount = 0
			# for Pos in FS.Area:
				# amount += self.food(Pos)
			# if amount != FS.FoodAmount:
				# print('************ error consistency %s: %d %d' % (FS.Name, amount, FS.FoodAmount))
				# print [self.food(Pos) for Pos in FS.Area]
				# FS.FoodAmount = amount

	def pick(self, Pos):
		# let the food source know
		f = None
		for FS in self.FoodSources:
			if Pos in FS.Area:
				Observer.recordChanges((FS.Name, FS.locate() + FoodDepletedAspect))	# to display food sources
				f = FS
				break
		if f:
			self.FoodSources.remove(f)
			return f.Name
		return None

	def put(self, name, Pos):
		if name:
			FS = FoodSource(name)
			FS.locate(Pos)
			for p in self.neighbours(FS.locate(), Radius=5):
				FS.Area.append(p)
			FS.FoodAmount = 10
			self.FoodSources += [FS]
		for FS in self.FoodSources:
			Observer.recordChanges((FS.Name, FS.locate() + (31 + 5*self.getNumberOfFS(FS.locate()), 14)))

	def getNumberOfFS(self, Pos, type = 0):
		count = 0
		for FS in self.FoodSources:
			if Pos in FS.Area: #TODO type
				count += 1
		return count

	def food(self, Pos, delta=0):
		if delta:
			# let the food source know-
			for FS in self.FoodSources:
				if Pos in FS.Area:
					FS.FoodAmount += delta
					if FS.FoodAmount <= 0:
						Observer.recordChanges((FS.Name, FS.locate() + FoodDepletedAspect))	# to display food sources
		return self.Cell(Pos).food(delta)	# adds food
	
	def foodQuantity(self):
		return sum([FS.FoodAmount for FS in self.FoodSources])
	
	def npheromone(self, Pos, delta=0):	
		if delta:	
			self.ActiveCells.append(Pos)
			Observer.recordChanges(('NP%d_%d' % Pos, Pos + NPAspect)) # for ongoing display of negative pheromone
		return self.Cell(Pos).np(delta)	# adds repulsive pheromone
		
	def ppheromone(self, Pos, delta=0):	
		if delta:
			self.ActiveCells.append(Pos)
			Observer.recordChanges(('PP%d_%d' % Pos, Pos + PPAspect)) # for ongoing display of positive pheromone
		return self.Cell(Pos).pp(delta)	# adds attractive pheromone

	def evaporation(self):
		for Pos in self.ActiveCells.list()[:]:
			if self.Cell(Pos).evaporate(): # no pheromone left
				# call 'erase' for updating display when there is no pheromone left
				self.erase(Pos) # for ongoing display
				self.ActiveCells.remove(Pos)

	def erase(self, Pos):
		" says to Observer that there is no pheromon left at that location "
		Observer.recordChanges(('NP%d_%d' % Pos, Pos + (-1,))) # negative colour means erase from display
		Observer.recordChanges(('PP%d_%d' % Pos, Pos + (-1,))) # negative colour means erase from display
		
	def update_(self):
		# scans ground for food and pheromone - May be used for statistics
		Food = NPher = PPher = []
		for (Position, Cell) in Land.travel():
			if Cell.Food:		Food.append((Pos, Cell.food()))
			if Cell.NPheromone:	NPher.append((Pos, Cell.np()))
			if Cell.PPheromone:	PPher.append((Pos, Cell.pp()))
		return (Food, NPher, PPher)
	   
	   
class Ant(EI.Individual):
	""" Defines individual agents
	"""
	def __init__(self, Scenario, IdNb, InitialPosition):
		EI.Individual.__init__(self, Scenario, ID=IdNb)
		self.Colony = InitialPosition # Location of the colony nest
		self.location = InitialPosition
		self.PPStock = Gbl.Parameter('PPMax') 
		self.Action = 'Move'
		self.laden = False
		self.ladenFSName = None
		self.moves()

	def getFoodDensity(self):
		Neighbourhood = Land.neighbours(self.location, self.Scenario.Parameter('PutSniffDistance'))
		c = 0
		for pos in Neighbourhood:
			c += int(Land.getNumberOfFS(pos) > 1)
		f = c / float(len(Neighbourhood))
		return f

	def getPutProbability(self):
		f = self.getFoodDensity()
		p = (f / float(putK + f))
		p *= p
		if p > 0 and p < 1:
			print 'put ' + ' ' + str(f) + ' => ' + str(p)
		return p

	def getPickProbability(self):
		f = self.getFoodDensity()
		p = (pickK / (pickK + f))
		p *= p
		if p > 0 and p < 1:
 			print 'pick ' + ' ' + str(f) + ' => ' + str(p)
		return p

	def Sniff(self):
		" Looks for the next place to go "
		Neighbourhood = Land.neighbours(self.location, self.Scenario.Parameter('SniffingDistance'))
		random.shuffle(Neighbourhood) # to avoid anisotropy
		acceptable = None
		best = -Gbl.Parameter('Saturation')	# best == pheromone balance found so far
		for NewPos in Neighbourhood:
			# looking for position devoid of negative pheromon
			if NewPos == self.location: continue

			# if not self.laden and self.getFoodDensity()> 0 and self.getFoodDensity()<1:
			# 	acceptable = NewPos
			# 	break

			# if self.laden and self.getFoodDensity()>0.7:
			# 	acceptable = NewPos
			# 	break

			found = Land.ppheromone(NewPos)   # attractiveness of positive pheromone
			found -= Land.npheromone(NewPos)   # repulsiveness of negative pheromone
			if found > best:			  
				acceptable = NewPos
				best = found
		return acceptable

	def moves(self):
		""" Basic behavior: move by looking for neighbouring unvisited cells.
			If food is in sight, return straight back home.
			Lay down negative pheromone on visited cells.
			Lay down positive pheromone on returning home.
		"""
		NextPos = self.Sniff()
		# print self.ID, 'in', self.location, 'sniffs', NextPos
		if NextPos is None or random.randint(0,100) < Gbl.Parameter('Exploration'): 
			# either all neighbouring cells have been visited or in the mood for exploration
			NextPos = c2t(t2c(self.location) + complex(random.randint(-1,1),random.randint(-1,1)))
			NextPos = Land.ToricConversion(NextPos)
		# Marking the old location as visited
		if Gbl.Parameter('NPDeposit'):
			Land.npheromone(self.location, Gbl.Parameter('NPDeposit'))
			# Observer.recordChanges(('NP%d_%d' % self.location, self.location + NPAspect)) # for ongoing display of negative pheromone
		self.location = NextPos

		if not self.laden:		
			#print 'pick ' + str(self.getPickProbability())
			if random.random() < self.getPickProbability():
				name = Land.pick(self.location)
				#Observer.FoodCollected += 1
				if name is not None:
					self.laden = True
					Observer.FoodCollected += 1
					self.ladenFSName = name + '_'

		aspect = AntAspect
		if self.laden == True:
			#print 'put ' + str(self.getPutProbability())
			if random.random() < self.getPutProbability():
				Land.put(self.ladenFSName, self.location)
				self.laden = False
				Observer.FoodCollected -= 1
			else:
				aspect = AntAspectWhenLaden
		Observer.recordChanges((self.ID, self.location + aspect)) # for ongoing display of ants

	def position(self):
		return c2t(self.Position)

		
class Group(EG.Group):
	# The group is a container for individuals.
	# Individuals are stored in self.members

	def __init__(self, Scenario, ColonyPosition, ID=1, Size=100):
		self.ColonyPosition = ColonyPosition
		EP.Group.__init__(self, Scenario, ID=ID, Size=Size)
		
	def createIndividual(self, ID=None, Newborn=True):
		# calling local class 'Individual'
		return Ant(self.Scenario, self.free_ID(Prefix='A'), self.ColonyPosition)	# call to local class 'Ant'
					
					
class Population(EP.Population):
	" defines the population of agents "
	
	def __init__(self, Scenario, Observer, ColonyPosition):
		self.ColonyPosition = ColonyPosition
		EP.Population.__init__(self, Scenario, Observer)
		" creates a population of ant agents "
		self.AllMoved = 0  # counts the number of times all agents have moved on average
		self.SimulationEnd = 4000 * self.popSize
		# allows to run on the simulation beyond stop condition

	def createGroup(self, ID=0, Size=0):
		return Group(self.Scenario, self.ColonyPosition, ID=ID, Size=Size)	# Call to local class 'Group'
		
	def One_Decision(self):
		""" This function is repeatedly called by the simulation thread.
			One ant is randomly chosen and decides what it does
		"""
		EP.Population.one_year(self)	# performs statistics
		ant = self.selectIndividual()
		ant.moves()
		Moves = self.year // self.popSize	# One step = all Walkers have moved once on average
		# print (self.year, self.AllMoved, Moves),
		if Moves > self.AllMoved:
			Land.evaporation()
			self.AllMoved = Moves
		if (Land.foodQuantity() <= 0):	self.SimulationEnd -= 1
		return self.SimulationEnd > 0	 # stops the simulation when True

		
if __name__ == "__main__":
	print __doc__

	#############################
	# Global objects			#
	#############################
	Gbl = EPar.Parameters('Params.evo')	# Loading global parameter values #TODO make it working with GUI
	Observer = Ant_Observer(Gbl)   # Observer contains statistics
	Land = Landscape(Gbl.Parameter('LandSize'), Gbl.Parameter('NbFoodSources'))
	Pop = Population(Gbl, Observer, (Gbl.Parameter('LandSize')//2, Gbl.Parameter('LandSize')//2))   # Ant colony
	Observer.recordChanges(('Dummy',(Gbl.Parameter('LandSize'), Gbl.Parameter('LandSize'), 0, 1)))	# to resize the field

	Observer.recordInfo('FieldWallpaper', 'Grass1.jpg')
	# Observer.recordInfo('FieldWallpaper', 'white')

	EW.Start(Pop.One_Decision, Observer, Capabilities='RPC')

	print "Bye......."
	sleep(1.0)
##	raw_input("\n[Return]")

__author__ = 'Dessalles'
